let gulpfile = require('gulp');
let less = require('gulp-less');
let path = require('path');
let cleanCSS = require('gulp-clean-css');

gulpfile.task('less', function () {
	return gulpfile.src('src/less/index.less')
		.pipe(less({
			paths: [ path.join(__dirname, 'less', 'includes') ]
		}))
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulpfile.dest('src/'));
});

gulpfile.task('watch', function() {
	gulpfile.watch('src/less/*.less', gulpfile.series('less'));
});
